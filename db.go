package main

import (
	"log"

	"github.com/jinzhu/gorm"

	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type User struct {
	gorm.Model
	Name     string `json:"name"`
	Email    string `gorm:"unique" json:"email"`
	Password string `json:"password"`
	Role     string `json:"role"`
}

type Authentication struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type Token struct {
	Name		string `json:"name"`
	Role        string `json:"role"`
	Email       string `json:"email"`
	TokenString string `json:"token"`
}

type TokenVerified struct {
	Name		string `json:"name"`
	Role        string `json:"role"`
	Email       string `json:"email"`
}

func GetDatabase() *gorm.DB {
	databasename := "d364cuhml30h8l"
	database := "postgres"
	databasepassword := "48810a7e5c2d3e1ee5d5b19bce9500df229e81bdea6fc9e255eae5e4077888c9"
	databaseurl := "postgres://vgyhlghhpjumgl:" + databasepassword + "@ec2-54-165-184-219.compute-1.amazonaws.com:5432/" + databasename + "?sslmode=require"
	connection, err := gorm.Open(database, databaseurl)
	if err != nil {
		log.Fatalln(err)
	}

	sqldb := connection.DB()

	err = sqldb.Ping()
	if err != nil {
		log.Fatal("database connected")
	}

	//  fmt.Println("connected to database")
	return connection
}
func InitialMigration() {
	connection := GetDatabase()
	defer Closedatabase(connection)
	connection.AutoMigrate(User{})
}

func Closedatabase(connection *gorm.DB) {
	sqldb := connection.DB()
	sqldb.Close()
}