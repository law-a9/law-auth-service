package main

import (
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	logrustash "github.com/bshuster-repo/logrus-logstash-hook"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"github.com/rs/cors"

	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
)

var logger *logrus.Logger
var router *mux.Router

// secretKey
var secretkey = os.Getenv("SECRET_KEY")

var connection *gorm.DB

func InitLogger() {
	logger = logrus.New()
	conn, err := tls.Dial("tcp", "2d6b8cd7-3358-43d4-b505-984212d4f971-ls.logit.io:20026", &tls.Config{ RootCAs: nil }) 
	if err != nil {
		log.Fatal(err)
	}
	hook := logrustash.New(conn, logrustash.DefaultFormatter(logrus.Fields{"type": "auth-service"}))
	logger.Hooks.Add(hook)
}

func CreateRouter() {
	router = mux.NewRouter()
}

func InitializeRoute() {
	router.HandleFunc("/signup", SignUp).Methods("POST")
	router.HandleFunc("/signin", SignIn).Methods("POST")
	router.HandleFunc("/verify", Verify).Methods("GET")
}

func ConnectDatabase() {
	connection = GetDatabase()
}

func SignUp(w http.ResponseWriter, r *http.Request) {
	var user User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		var err error
		err = errors.New("Error in reading body")
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(err)
		return
	}
	var dbuser User
	connection.Where("email = ?", user.Email).First(&dbuser)

	//checks if email is already register or not
	if dbuser.Email != "" {
		var err error
		err = errors.New("Email already in use")
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(err)
		return
	}

	user.Password, err = GeneratehashPassword(user.Password)
	if err != nil {
		log.Fatalln("error in password hash")
	}

	//insert user details in database
	connection.Create(&user)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(user)
}

func SignIn(w http.ResponseWriter, r *http.Request) {
	var authdetails Authentication
	err := json.NewDecoder(r.Body).Decode(&authdetails)
	if err != nil {
		var err error
		err = errors.New("Error in reading body")
		ctx := logger.WithFields(logrus.Fields{
			"method":"SignIn",
		})
		ctx.Error(err.Error())
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(err)
		return
	}

	var authuser User
	connection.Where("email = ?", authdetails.Email).First(&authuser)
	if authuser.Email == "" {
		var err error
		err = errors.New("Username or Password is incorrect")
		ctx := logger.WithFields(logrus.Fields{
			"method":"SignIn",
		})
		ctx.Error(err.Error())
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(err)
		return
	}

	check := CheckPasswordHash(authdetails.Password, authuser.Password)

	if !check {
		var err error
		err = errors.New("Username or Password is incorrect")
		ctx := logger.WithFields(logrus.Fields{
			"method":"SignIn",
		})
		ctx.Error(err.Error())
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(err)
		return
	}

	validToken, err := GenerateJWT(authuser.Name, authuser.Email, authuser.Role)
	if err != nil {
		var err error
		err = errors.New("Failed to generate token")
		ctx := logger.WithFields(logrus.Fields{
			"method":"SignIn",
		})
		ctx.Error(err.Error())
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(err)
		return
	}

	var token Token
	token.Name = authuser.Name
	token.Email = authuser.Email
	token.Role = authuser.Role
	token.TokenString = validToken
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(token)
	ctx := logger.WithFields(logrus.Fields{
		"method":"SignIn",
	})
	ctx.Info("User " + authuser.Name + " logged in")
}

func Verify(w http.ResponseWriter, r *http.Request) {
	if r.Header["Token"] == nil {
		var err error
		err = errors.New("No Token Found")
		ctx := logger.WithFields(logrus.Fields{
			"method":"Verify",
		})
		ctx.Error(err.Error())
		json.NewEncoder(w).Encode(err)
		return
	}
	
	var mySigningKey = []byte(secretkey)
	
	token, err := jwt.Parse(r.Header["Token"][0], func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			ctx := logger.WithFields(logrus.Fields{
				"method":"SignIn",
				"token":token,
			})
			ctx.Error("There was an error in parsing")
			return nil, fmt.Errorf("There was an error in parsing")
		}
		return mySigningKey, nil
	})
	
	if err != nil {
		var err error
		err = errors.New("Your Token has been expired")
		ctx := logger.WithFields(logrus.Fields{
			"method":"Verify",
		})
		ctx.Error(err.Error())
		json.NewEncoder(w).Encode(err)
		return
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		if claims["role"] == "dokter" {
			var token TokenVerified
			token.Name = fmt.Sprintf("%v", claims["name"])
			token.Email = fmt.Sprintf("%v", claims["email"])
			token.Role = fmt.Sprintf("%v", claims["role"])
			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(token)
			ctx := logger.WithFields(logrus.Fields{
				"method":"Verify",
			})
			ctx.Info("User " + token.Name + " verified")
			return
		} else if claims["role"] == "pasien" {
			var token TokenVerified
			token.Name = fmt.Sprintf("%v", claims["name"])
			token.Email = fmt.Sprintf("%v", claims["email"])
			token.Role = fmt.Sprintf("%v", claims["role"])
			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(token)
			ctx := logger.WithFields(logrus.Fields{
				"method":"Verify",
			})
			ctx.Info("User " + token.Name + " verified")
			return
		}
	}
	
	var reserr error
	reserr = errors.New("Not Authorized")
	json.NewEncoder(w).Encode(reserr)
	ctx := logger.WithFields(logrus.Fields{
		"method":"Verify",
	})
	ctx.Error(reserr.Error())
}

func GeneratehashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func GenerateJWT(name, email, role string) (string, error) {
	var mySigningKey = []byte(secretkey)
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)

	claims["authorized"] = true
	claims["email"] = email
	claims["role"] = role
	claims["exp"] = time.Now().Add(time.Minute * 20).Unix()
	claims["name"] = name

	tokenString, err := token.SignedString(mySigningKey)



	if err != nil {
		fmt.Errorf("Something Went Wrong: %s", err.Error())
		ctx := logger.WithFields(logrus.Fields{
			"method":"GenerateJWT",
		})
		ctx.Error(err.Error())
		return "", err
	}
	return tokenString, nil
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func main() {
	InitLogger()
	InitialMigration()
	ConnectDatabase()
	CreateRouter()
	InitializeRoute()
	handler := cors.AllowAll().Handler(router);
	ctx := logger.WithFields(logrus.Fields{
		"method":"main",
	})
	ctx.Info("Auth Service started")
	log.Fatal(http.ListenAndServe(":" + os.Getenv("PORT"), handler))
}